# Requirements: #

PHP 5.4+
Composer

### Set Up ###

* Run 'composer update' after cloning to install all dependencies.
* Add your hostname to 'local' array in bootstrap/start.php file.
* Configure database setup inside config/local/database.php file.

### Run ###
* Run 'php artisan serve' on the root of the folder.