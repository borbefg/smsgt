<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaptops extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('laptops', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('brand');
			$table->string('model');
			$table->string('processor');
			$table->string('memory');
			$table->string('storage');
			$table->string('serial_no');
			$table->string('ethernet_mac')->nullable();
			$table->string('wireless_mac')->nullable();
			$table->string('asset_tag')->unique();
			$table->string('os')->nullable();
			$table->string('os_product_key')->nullable();
			$table->string('office')->nullable();
			$table->string('office_product_key')->nullable();
			$table->string('condition');
			$table->string('status');
			$table->text('accessories')->nullable();
			// $table->date('date_delivered');
			// $table->date('date_issued');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('laptops');
	}

}
