<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployees extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table) {
			$table->increments('id');
			$table->string('employer', 50);
			$table->string('id_number')->unique();
			$table->string('first_name', 100);
			$table->string('middle_name', 100);
			$table->string('last_name', 100);
			$table->string('date_hired');
			$table->string('contract_period')->nullable();
			$table->string('probitionary_date')->nullable();
			$table->string('permanency_date')->nullable();
			$table->string('designation');
			$table->string('department');
			$table->string('email')->nullable();
			$table->string('contact_number');
			$table->string('gender', 20);
			$table->string('civil_status', 20);
			$table->string('dependents');
			$table->string('hmo')->nullable();
			$table->string('sss')->nullable();
			$table->string('philhealth')->nullable();
			$table->string('pagibig')->nullable();
			$table->string('tin')->nullable();
			$table->string('date_of_birth');
			$table->text('home_address');
			$table->string('emergency_contact')->nullable();
			$table->string('emergency_number')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
