<?php

class Memo extends Eloquent {

	protected $table = 'memos';

	protected $guarded = array();

	public static $rules = array(
		'laptop_id' => 'required',
		'employee_id' => 'required',
		'date_issued' => 'required',
		'date_returned' => ''
	);

	public function employee()
	{
		return $this->belongsTo('Employee');
	}

	public function laptop()
	{
		return $this->belongsTo('Laptop');
	}
}
