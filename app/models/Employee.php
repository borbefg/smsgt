<?php

class Employee extends Eloquent {

	protected $table = 'employees';

	protected $guarded = array();

	public static $rules = array(
		'employer'			=> 'required',
		'id_number'			=> 'required',
		'first_name'		=> 'required',
		'middle_name'		=> 'required',
		'last_name'			=> 'required',
		'date_hired'		=> 'required',
		'contract_period'	=> '',
		'probitionary_date'	=> '',
		'permanency_date'	=> '',
		'designation'		=> 'required',
		'department'		=> 'required',
		'email'				=> '',
		'contact_number'	=> '',
		'gender'			=> '',
		'civil_status'		=> '',
		'dependents'		=> '',
		'hmo'				=> '',
		'sss'				=> '',
		'philhealth'		=> '',
		'pagibig'			=> '',
		'tin'				=> '',
		'date_of_birth'		=> '',
		'home_address'		=> '',
		'emergency_contact'	=> '',
		'emergency_number'	=> ''
	);

	public function memos()
	{
		return $this->hasMany('Memo');
	}
}
