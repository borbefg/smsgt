<?php

class Laptop extends Eloquent {

	protected $table = 'laptops';

	protected $guarded = array();

	public static $rules = array(
		'name'					=> '',
		'brand'					=> '',
		'model'					=> '',
		'processor'				=> '',
		'memory'				=> '',
		'storage'				=> '',
		'serial_no'				=> '',
		'ethernet_mac'			=> '',
		'wireless_mac'			=> '',
		'asset_tag'				=> '',
		'os'					=> '',
		'os_product_key'		=> '',
		'office'				=> '',
		'office_product_key'	=> '',
		'condition'				=> '',
		'status'				=> '',
		'accessories'			=> ''
	);

	public function memos()
	{
		return $this->hasMany('memos');
	}
}
