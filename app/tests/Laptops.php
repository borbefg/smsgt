<?php

class Laptops extends TestCase {

	public function testNewLaptop()
	{
		$laptop = new Laptop;
		$laptop->name				= 'Lydia';
		$laptop->brand				= 'ASUS';
		$laptop->model				= 'G750';
		$laptop->processor			= 'Intel Core i7';
		$laptop->memory				= '16 GB';
		$laptop->storage			= '1 TB';
		$laptop->serial_no			= '8594228B3NQ';
		$laptop->ethernet_mac		= '64:B9:E8:5B:64:51';
		$laptop->wireless_mac		= '64:B9:E8:5B:64:50';
		$laptop->asset_tag			= '012025001531477';
		$laptop->os					= 'Windows 8.1 Pro';
		$laptop->os_product_key		= '129U4-VN90Q-JEM1B-4U0QW-EJV0Q';
		$laptop->office				= 'Microsoft Office 2013';
		$laptop->office_product_key	= '098TY-UGHNB-OI9Y7-T6FCG-T6879';
		$laptop->condition			= 'Brand New';
		$laptop->status				= 'Available';
		$laptop->accessories		= 'Backpack';

		// laptop should save
		$this->assertTrue($laptop->save());

		// $this->assertTrue(true);
		// $this->assertFalse(false);
		// $this->assertSame('i am a string.' ,'i am a string.');
		// $this->assertContains('am', 'i am a string');
		// $this->assertArrayHasKey('key', array('key'=>'val'));
	}

}
