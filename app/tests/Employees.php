<?php

class Employees extends TestCase {

	public function test()
	{
	  // Create a new Employee
	  $employee = new Employee;
	  $employee->employer 			= 'SMS';
	  $employee->id_number 			= '01-0001';
	  $employee->first_name 		= 'Francis';
	  $employee->middle_name 		= 'Guarino';
	  $employee->last_name			= 'Borbe';
	  $employee->date_hired			= '18 Jul 2014';
	  $employee->contract_period 	= '18 Jul 2014 - 18 Jul 2015';
	  $employee->probitionary_date 	= '18 Jul 2014';
	  $employee->permanency_date	= '18 Jul 2014';
	  $employee->designation		= 'Intern';
	  $employee->department			= 'IT Department';
	  $employee->email 				= 'borbefg@gmail.com';
	  $employee->contact_number		= '(+63) 936-100-0478';
	  $employee->gender 			= 'Male';
	  $employee->civil_status		= 'Single';
	  $employee->dependents			= '0';
	  $employee->hmo 				= '';
	  $employee->sss 				= '';
	  $employee->philhealth			= '';
	  $employee->pagibig			= '';
	  $employee->tin 				= '';
	  $employee->date_of_birth		= '08 Jan 1991';
	  $employee->home_address		= '#24 Mapangakit Street, Pinyahan, Quezon City';
	  $employee->emergency_contact	= '';
	  $employee->emergency_number	= '';

	  // employee should not save
	  $this->assertTrue($employee->save());
	 
	  // Save the errors
	  $errors = $employee->errors()->all();
	 
	  // There should be 1 error
	  $this->assertCount(1, $errors);
	 
	}

}