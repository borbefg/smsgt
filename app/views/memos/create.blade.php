@extends('layouts.scaffold')

@section('main')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create Memo</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'memos.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('laptop_id', 'Laptop_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('laptop_id', Input::old('laptop_id'), array('class'=>'form-control', 'placeholder'=>'Laptop_id')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('employee_id', 'Employee_id:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('employee_id', Input::old('employee_id'), array('class'=>'form-control', 'placeholder'=>'Employee_id')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('date_issued', 'Date_issued:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('date_issued', Input::old('date_issued'), array('class'=>'form-control', 'placeholder'=>'Date_issued')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('date_returned', 'Date_returned:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('date_returned', Input::old('date_returned'), array('class'=>'form-control', 'placeholder'=>'Date_returned')) }}
            </div>
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

@stop


