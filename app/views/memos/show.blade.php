@extends('layouts.scaffold')

@section('main')

<h1>Show Memo</h1>

<p>{{ link_to_route('memos.index', 'Return to All memos', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Laptop_id</th>
				<th>Employee_id</th>
				<th>Date_issued</th>
				<th>Date_returned</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $memo->laptop->name }}}</td>
			<td>{{{ $memo->employee->first_name }}}</td>
			<td>{{{ $memo->date_issued }}}</td>
			<td>{{{ $memo->date_returned }}}</td>
            <td>
                {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('memos.destroy', $memo->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
                {{ link_to_route('memos.edit', 'Edit', array($memo->id), array('class' => 'btn btn-info')) }}
            </td>
		</tr>
	</tbody>
</table>

@stop
