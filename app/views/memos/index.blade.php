@extends('layouts.scaffold')

@section('main')

<h1 class="">
	<span class="pull-left">
        <a href="/dashboard" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
	{{ $heading }} <small>{{ $sub_heading }}</small>
	<span class="pull-right">
		 <a href="/memos/create" class="btn btn-default"><i class="fa fa-plus fa-fw"></i></a>
    </span>
</h1>
<hr>

@if ($memos->count())
	<table class="table table-bordered table-hover table-condensed data-table">
		<thead>
			<tr>
				<th>Trace Number</th>
                <th>Employee</th>
                <th>Laptop</th>
				<th>Date Issued</th>
				<th>Date Returned</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($memos as $memo)
				<tr>
					<td>{{ link_to_route('memos.show', strtotime($memo->created_at).'-'.$memo->id, array($memo->id)) }}</td>
					<td>{{{ $memo->employee->first_name }}}</td>
					<td>{{{ $memo->laptop->serial_no }}}</td>
                    <td>{{{ $memo->date_issued }}}</td>
					<td>{{{ $memo->date_returned }}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no memos
@endif

@stop
