@extends('layouts.scaffold')

@section('main')

<h1 class="">
    <span class="pull-left">
         <!-- <a href="/laptops/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a> -->
        <a href="/employees" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
    {{ $heading }} <small>{{ $sub_heading }}</small>
    <span class="pull-right">
         <!-- <a href="/employees/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a> -->
    </span>
</h1>
<hr>

<!-- <div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h1>New Employee</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div> -->

{{ Form::open(array('route' => 'employees.store', 'class' => 'form-horizontal')) }}

<!-- <div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="col-md-5">
            

        </div>

        <div class="col-md-5"></div>
    </div>
</div> -->

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('employer', 'Employer:', array('class'=>'control-label')) }}
            {{ Form::text('employer', Input::old('employer'), array('class'=>'form-control', 'placeholder'=>'SMS | AIM')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('first_name', 'First Name:', array('class'=>'control-label')) }}
            {{ Form::text('first_name', Input::old('first_name'), array('class'=>'form-control', 'placeholder'=>'First Name')) }}
        </div> 
        <div class="col-xs-3">
            {{ Form::label('middle_name', 'Middle Name:', array('class'=>'control-label')) }}
            {{ Form::text('middle_name', Input::old('middle_name'), array('class'=>'form-control', 'placeholder'=>'Middle Name')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('last_name', 'Last Name:', array('class'=>'control-label')) }}
            {{ Form::text('last_name', Input::old('last_name'), array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('id_number', 'ID Number:', array('class'=>'control-label')) }}
            {{ Form::text('id_number', Input::old('id_number'), array('class'=>'form-control', 'data-mask'=>'99-999', 'placeholder'=>'00-000')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('date_hired', 'Date Hired:', array('class'=>'control-label')) }}
            {{ Form::text('date_hired', Input::old('date_hired'), array('class'=>'form-control datepicker')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('probitionary_date', 'Probitionary Date:', array('class'=>'control-label')) }}
            {{ Form::text('probitionary_date', Input::old('probitionary_date'), array('class'=>'form-control datepicker')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('permanency_date', 'Permanency Date:', array('class'=>'control-label')) }}
            {{ Form::text('permanency_date', Input::old('permanency_date'), array('class'=>'form-control datepicker')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('contract_period', 'Contract Period:', array('class'=>'control-label')) }}
            {{ Form::text('contract_period', Input::old('contract_period'), array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('designation', 'Designation:', array('class'=>'control-label')) }}
            {{ Form::text('designation', Input::old('designation'), array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('department', 'Department:', array('class'=>'control-label')) }}
            {{ Form::text('department', Input::old('department'), array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('email', 'Email:', array('class'=>'control-label')) }}
            {{ Form::text('email', Input::old('email'), array('class'=>'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('contact_number', 'Contact Number:', array('class'=>'control-label')) }}
            {{ Form::text('contact_number', Input::old('contact_number'), array('class'=>'form-control', 'data-mask'=>'(+63) 999-999-9999')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('gender', 'Gender:', array('class'=>'control-label')) }}
            {{ Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), Input::old('gender'), array('class'=>'form-control chosen-select')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('civil_status', 'Civil Status:', array('class'=>'control-label')) }}
            {{ Form::select('civil_status', array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Annulled' => 'Annulled'), Input::old('civil_status'), array('class'=>'form-control chosen-select')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('dependents', 'Dependents:', array('class'=>'control-label')) }}
            {{ Form::select('dependents', array('None'=>'None', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'), Input::old('dependents'), array('class'=>'form-control chosen-select')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('hmo', 'HMO:', array('class'=>'control-label')) }}
            {{ Form::text('hmo', Input::old('hmo'), array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('sss', 'SSS:', array('class'=>'control-label')) }}
            {{ Form::text('sss', Input::old('sss'), array('class'=>'form-control', 'data-mask'=>'99-9999999-9')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('philhealth', 'Phil Health:', array('class'=>'control-label')) }}
            {{ Form::text('philhealth', Input::old('philhealth'), array('class'=>'form-control', 'data-mask'=>'99-999999999-9')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('pagibig', 'Pag-IBIG:', array('class'=>'control-label')) }}
            {{ Form::text('pagibig', Input::old('pagibig'), array('class'=>'form-control', 'data-mask'=>'9999-9999-9999')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('tin', 'TIN:', array('class'=>'control-label')) }}
            {{ Form::text('tin', Input::old('tin'), array('class'=>'form-control', 'data-mask'=>'999-999-999')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('date_of_birth', 'Date of Birth:', array('class'=>'control-label')) }}
            {{ Form::text('date_of_birth', Input::old('date_of_birth'), array('class'=>'form-control datepicker')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('emergency_contact', 'Emergency Contact:', array('class'=>'control-label')) }}
            {{ Form::text('emergency_contact', Input::old('emergency_contact'), array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('emergency_number', 'Emergency Number:', array('class'=>'control-label')) }}
            {{ Form::text('emergency_number', Input::old('emergency_number'), array('class'=>'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            {{ Form::label('home_address', 'Home Address:', array('class'=>'control-label')) }}
            {{ Form::textarea('home_address', Input::old('home_address'), array('class'=>'form-control', 'rows'=>'3')) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-12 control-label">&nbsp;</label>
        <div class="col-xs-12">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>

{{ Form::close() }}

@stop


