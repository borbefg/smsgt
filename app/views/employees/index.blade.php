@extends('layouts.scaffold')
@section('main')

<h1 class="">
	<span class="pull-left">
        <a href="/dashboard" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
	{{ $heading }} <small>{{ $sub_heading }}</small>
	<span class="pull-right">
		 <a href="/employees/create" class="btn btn-default"><i class="fa fa-plus fa-fw"></i></a>	    
    </span>
</h1>
<hr>

@if ($employees->count())
	<table class="table table-bordered table-hover table-condensed data-table">
		<thead>
			<tr>
				<th>Full Name</th>
				<th>ID Number</th>
				<th>Employer</th>
				<th>Designation</th>
				<th>Department</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($employees as $employee)
				<tr>
					<td>{{ link_to_route('employees.show', $employee->last_name.', '.$employee->first_name.' '.$employee->middle_name, array($employee->id)) }}</td>
					<td>{{{ $employee->id_number }}}</td>
					<td>{{{ $employee->employer }}}</td>
					<td>{{{ $employee->designation }}}</td>
					<td>{{{ $employee->department }}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no employees
@endif

@stop
