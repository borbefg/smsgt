@extends('layouts.scaffold')

@section('main')

<h1 class="">
    <span class="pull-left">
         <!-- <a href="/laptops/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a> -->
        <a href="/employees" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
    {{ $heading }} <small>{{ $sub_heading }}</small>
    <span class="pull-right">
        {{ link_to_route('employees.edit', 'Edit', array($employee->id), array('class' => 'btn btn-flat btn-default')) }}
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('employees.destroy', $employee->id))) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-flat btn-danger')) }}
        {{ Form::close() }}
    </span>
</h1>
<hr>

<!-- <h1 class="page-header">{{ $employee->last_name.', '.$employee->first_name.' '.$employee->middle_name }} -->
	<!--  link_to_route('employees.create', <i class="fa fa-plus-sign"></i>, null, array('class' => 'btn btn-success pull-right')) }} -->
	<!-- <a href="/employees" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw">&nbsp;</i>Show All Employees</a> -->
<!-- </h1> -->
<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
        <li><a href="#memos" data-toggle="tab">Memos</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="profile">
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-3">
                        {{ Form::label('employer', 'Employer:', array('class'=>'control-label')) }}
                        {{ Form::label('employer', $employee->employer, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('first_name', 'First Name:', array('class'=>'control-label')) }}
                        {{ Form::label('first_name', $employee->first_name, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('middle_name', 'Middle Name:', array('class'=>'control-label')) }}
                        {{ Form::label('middle_name', $employee->middle_name, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('last_name', 'Last Name:', array('class'=>'control-label')) }}
                        {{ Form::label('last_name', $employee->last_name, array('class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-3">
                        {{ Form::label('id_number', 'ID Number:', array('class'=>'control-label')) }}
                        {{ Form::label('id_number', $employee->id_number, array('class'=>'form-control', 'data-mask'=>'99-999')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('date_hired', 'Date Hired:', array('class'=>'control-label')) }}
                        {{ Form::label('date_hired', $employee->date_hired, array('class'=>'form-control datepicker')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('probitionary_date', 'Probitionary Date:', array('class'=>'control-label')) }}
                        {{ Form::label('probitionary_date', $employee->probitionary_date, array('class'=>'form-control datepicker')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('permanency_date', 'Permanency Date:', array('class'=>'control-label')) }}
                        {{ Form::label('permanency_date', $employee->permanency_date, array('class'=>'form-control datepicker')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-3">
                        {{ Form::label('contract_period', 'Contract Period:', array('class'=>'control-label')) }}
                        {{ Form::label('contract_period', $employee->contract_period, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('designation', 'Designation:', array('class'=>'control-label')) }}
                        {{ Form::label('designation', $employee->designation, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('department', 'Department:', array('class'=>'control-label')) }}
                        {{ Form::label('department', $employee->department, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('email', 'Email:', array('class'=>'control-label')) }}
                        {{ Form::label('email', $employee->email, array('class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-3">
                        {{ Form::label('contact_number', 'Contact Number:', array('class'=>'control-label')) }}
                        {{ Form::label('contact_number', $employee->contact_number, array('class'=>'form-control', 'data-mask'=>'(+63) 999-999-9999')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('gender', 'Gender:', array('class'=>'control-label')) }}
                        {{ Form::label('gender', $employee->gender, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('civil_status', 'Civil Status:', array('class'=>'control-label')) }}
                        {{ Form::label('civil_status', $employee->civil_status, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('dependents', 'Dependents:', array('class'=>'control-label')) }}
                        {{ Form::label('dependents', $employee->dependents, array('class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-3">
                        {{ Form::label('hmo', 'HMO:', array('class'=>'control-label')) }}
                        {{ Form::label('hmo', $employee->hmo, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('sss', 'SSS:', array('class'=>'control-label')) }}
                        {{ Form::label('sss', $employee->sss, array('class'=>'form-control', 'data-mask'=>'99-9999999-9')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('philhealth', 'Phil Health:', array('class'=>'control-label')) }}
                        {{ Form::label('philhealth', $employee->philhealth, array('class'=>'form-control', 'data-mask'=>'99-999999999-9')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('pagibig', 'Pag-IBIG:', array('class'=>'control-label')) }}
                        {{ Form::label('pagibig', $employee->pagibig, array('class'=>'form-control', 'data-mask'=>'9999-9999-9999')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-3">
                        {{ Form::label('tin', 'TIN:', array('class'=>'control-label')) }}
                        {{ Form::label('tin', $employee->tin, array('class'=>'form-control', 'data-mask'=>'999-999-999-999')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('date_of_birth', 'Date of Birth:', array('class'=>'control-label')) }}
                        {{ Form::label('date_of_birth', $employee->date_of_birth, array('class'=>'form-control datepicker')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('emergency_contact', 'Emergency Contact:', array('class'=>'control-label')) }}
                        {{ Form::label('emergency_contact', $employee->emergency_contact, array('class'=>'form-control')) }}
                    </div>
                    <div class="col-xs-3">
                        {{ Form::label('emergency_number', 'Emergency Number:', array('class'=>'control-label')) }}
                        {{ Form::label('emergency_number', $employee->emergency_number, array('class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        {{ Form::label('home_address', 'Home Address:', array('class'=>'control-label')) }}
                        {{ Form::textarea('home_address', $employee->home_address, array('class'=>'form-control', 'rows'=>'3', 'disabled')) }}
                    </div>
                </div>
            </form>
        </div><!-- /.tab-pane -->

        <div class="tab-pane" id="memos">
            @if ($employee->memos)
                <table class="table table-bordered table-hover table-condensed data-table">
                    <thead>
                        <tr>
                            <th>Device</th>
                            <th>Asset Tag</th>
                            <th>Date Issued</th>
                            <th>Date Returned</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($employee->memos as $memo)
                            <tr>
                                <td>{{ link_to_route('laptops.show', $memo->laptop->name, $memo->laptop->id) }}</td>
                                <td>{{{ $memo->laptop->asset_tag }}}</td>
                                <td>{{{ $memo->date_issued }}}</td>
                                <td>{{{ $memo->date_returned }}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                There are no employees
            @endif

        </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
</div><!-- nav-tabs-custom -->




@stop
