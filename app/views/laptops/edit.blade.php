@extends('layouts.scaffold')

@section('main')

<h1 class="">
    <span class="pull-left">
         <!-- <a href="/laptops/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a> -->
        <a href="/laptops/{{ $laptop->id }}" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
    {{ $laptop->name }} <small>{{ $sub_heading }}</small>
    <span class="pull-right">
        <button class="btn btn-default btn-flat" type="submit" form="laptops_edit_form"><i class="fa fa-save fa-fw">&nbsp;</i></button>
        {{ link_to_route('laptops.show', 'Cancel', $laptop->id, array('class' => 'btn btn-default')) }}
    </span>
</h1>
<hr>
<!-- 
<div class="row">
    <h1 class="page-header">
        <a href="/laptops/{{ $laptop->id }}" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
        {{ $laptop->name}}
    </h1>

    @if ($errors->any())
    	<div class="alert alert-danger">
    	    <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </ul>
    	</div>
    @endif
</div> -->

{{ Form::model($laptop, array('id'=>'laptops_edit_form', 'class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('laptops.update', $laptop->id))) }}

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('name', 'Name:', array('class'=>'control-label')) }}
            {{ Form::text('name', Input::old('name'), array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('brand', 'Brand:', array('class'=>'control-label')) }}
            {{ Form::text('brand', Input::old('brand'), array('class'=>'form-control', 'placeholder'=>'Brand')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('model', 'Model:', array('class'=>'control-label')) }}
            {{ Form::text('model', Input::old('model'), array('class'=>'form-control', 'placeholder'=>'Model')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('serial_no', 'Serial No:', array('class'=>'control-label')) }}
            {{ Form::text('serial_no', Input::old('serial_no'), array('class'=>'form-control', 'placeholder'=>'Serial No')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('processor', 'Processor:', array('class'=>'control-label')) }}
            {{ Form::text('processor', Input::old('processor'), array('class'=>'form-control', 'placeholder'=>'Processor')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('memory', 'Memory:', array('class'=>'control-label')) }}
            {{ Form::text('memory', Input::old('memory'), array('class'=>'form-control', 'placeholder'=>'Memory')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('storage', 'Storage:', array('class'=>'control-label')) }}
            {{ Form::text('storage', Input::old('storage'), array('class'=>'form-control', 'placeholder'=>'Storage')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('asset_tag', 'Asset Tag:', array('class'=>'control-label')) }}
            {{ Form::text('asset_tag', Input::old('asset_tag'), array('class'=>'form-control', 'placeholder'=>'Asset Tag')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('os', 'OS:', array('class'=>'control-label')) }}
            {{ Form::text('os', Input::old('os'), array('class'=>'form-control', 'placeholder'=>'OS')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('os_product_key', 'OS Product Key:', array('class'=>'control-label')) }}
            {{ Form::text('os_product_key', Input::old('os_product_key'), array('class'=>'form-control', 'data-mask'=>'wwwww-wwwww-wwwww-wwwww-wwwww', 'placeholder'=>'12345-67890-ABCDE-FGHIJ-KLMNO')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('office', 'Office:', array('class'=>'control-label')) }}
            {{ Form::text('office', Input::old('office'), array('class'=>'form-control', 'placeholder'=>'Office')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('office_product_key', 'Office Product Key:', array('class'=>'control-label')) }}
            {{ Form::text('office_product_key', Input::old('office_product_key'), array('class'=>'form-control', 'data-mask'=>'wwwww-wwwww-wwwww-wwwww-wwwww', 'placeholder'=>'12345-67890-ABCDE-FGHIJ-KLMNO')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('ethernet_mac', 'Ethernet Mac:', array('class'=>'control-label')) }}
            {{ Form::text('ethernet_mac', Input::old('ethernet_mac'), array('class'=>'form-control', 'data-mask'=>'ww:ww:ww:ww:ww:ww', 'placeholder'=>'00:00:00:00:00:00')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('wireless_mac', 'Wireless Mac:', array('class'=>'control-label')) }}
            {{ Form::text('wireless_mac', Input::old('wireless_mac'), array('class'=>'form-control', 'data-mask'=>'ww:ww:ww:ww:ww:ww', 'placeholder'=>'00:00:00:00:00:00')) }}
        </div>
    
        <div class="col-xs-3">
            {{ Form::label('condition', 'Condition:', array('class'=>'control-label')) }}<br>
            <!-- {{ Form::text('condition', Input::old('condition'), array('class'=>'form-control', 'placeholder'=>'e.g. Brand New, Used, Spare')) }} -->
            {{ Form::select('condition', array(
                'Brand New'=>'Brand New',
                'Good Condition'=>'Good Condition',
                'Usable but with defect(s)'=>'Usable but with defect(s)',
                'Unusable'=>'Unusable',
                ), Input::old('condition'), array('class'=>'form-control chosen-select'))
            }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('status', 'Status:', array('class'=>'control-label')) }}<br>
            <!-- {{ Form::text('status', Input::old('status'), array('class'=>'form-control', 'placeholder'=>'e.g. Available, Issued')) }} -->
            {{ Form::select('status', array('Available'=>'Available', 'Issued'=>'Issued'), Input::old('status'), array('class'=>'form-control chosen-select')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {{ Form::label('accessories', 'Accessories:', array('class'=>'control-label')) }}
            {{ Form::textarea('accessories', Input::old('accessories'), array('class'=>'form-control', 'placeholder'=>'e.g. Backpack, Mouse, USB Flash Drive, etc.')) }}
        </div>
    </div>
{{ Form::close() }}

@stop