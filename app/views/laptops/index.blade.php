@extends('layouts.scaffold')

@section('main')

<h1 class="">
	<span class="pull-left">
         <!-- <a href="/laptops/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a> -->
        <a href="/dashboard" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
	{{ $heading }} <small>{{ $sub_heading }}</small>
	<span class="pull-right">
		 <a href="/laptops/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a>	    
    </span>
</h1>
<hr>

@if ($laptops->count())
	<table class="table table-bordered table-hover table-condensed data-table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Brand</th>
				<th>Model</th>
				<th>Serial No</th>
				<th>Asset Tag</th>
				<th>Condition</th>
				<th>Status</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($laptops as $laptop)
				<tr>
					<td>{{ link_to_route('laptops.show', $laptop->name, array($laptop->id)) }}</td>
					<td>{{{ $laptop->brand }}}</td>
					<td>{{{ $laptop->model }}}</td>
					<td>{{{ $laptop->serial_no }}}</td>
					<td>{{{ $laptop->asset_tag }}}</td>
					<td>{{{ $laptop->condition }}}</td>
					<td>{{{ $laptop->status }}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no laptops
@endif

@stop
