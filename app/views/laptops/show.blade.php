@extends('layouts.scaffold')
@section('main')

<h1 class="">
    <span class="pull-left">
         <!-- <a href="/laptops/create" class="btn btn-flat btn-default"><i class="fa fa-plus fa-fw"></i></a> -->
        <a href="/laptops" class="pull-left"><i class="fa fa-angle-left fa-fw">&nbsp;</i></a>
    </span>
    {{ $laptop->name }} <small>{{ $sub_heading }}</small>
    <span class="pull-right">
        <?php if($laptop->status === 'Available'){ ?>
            <a href="javascript:void(0)" class="btn btn-flat btn-default"><i class="fa fa-check-square fa-fw" data-toggle="modal" data-target="#issue_modal"></i></a>
        <?php } else { ?>
            <a href="/memos/create/{{ $laptop->id }}" class="btn btn-flat btn-default"><i class="fa fa-minus-square fa-fw"></i></a>
        <?php } ?>
        <!-- {{ link_to_route('laptops.edit', 'Edit', array($laptop->id), array('class' => 'btn btn-flat btn-default')) }} -->
        <a href="/laptops/{{ $laptop->id }}/edit" class="btn btn-flat btn-default"><i class="fa fa-edit"></i></a>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('laptops.destroy', $laptop->id))) }}
            <button class="btn btn-flat btn-danger" type="submit"><i class="fa fa-trash fa-fw">&nbsp;</i></button>
        {{ Form::close() }}
    </span>
</h1>
<hr>

<form class="form-horizontal">
 	<div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('name', 'Name:', array('class'=>'control-label')) }}
            {{ Form::label('name', $laptop->name, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('brand', 'Brand:', array('class'=>'control-label')) }}
            {{ Form::label('brand', $laptop->brand, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('model', 'Model:', array('class'=>'control-label')) }}
            {{ Form::label('model', $laptop->model, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('serial_no', 'Serial No:', array('class'=>'control-label')) }}
            {{ Form::label('serial_no', $laptop->serial_no, array('class'=>'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('processor', 'Processor:', array('class'=>'control-label')) }}
            {{ Form::label('processor', $laptop->processor, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('memory', 'Memory:', array('class'=>'control-label')) }}
            {{ Form::label('memory', $laptop->memory, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('storage', 'Storage:', array('class'=>'control-label')) }}
            {{ Form::label('storage', $laptop->storage, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('asset_tag', 'Asset Tag:', array('class'=>'control-label')) }}
            {{ Form::label('asset_tag', $laptop->asset_tag, array('class'=>'form-control')) }}
        </div>
    </div>

    <div class="form-group">   
        <div class="col-xs-3">
            {{ Form::label('os', 'OS:', array('class'=>'control-label')) }}
            {{ Form::label('os', $laptop->os, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('os_product_key', 'OS Product Key:', array('class'=>'control-label')) }}
            {{ Form::label('os_product_key', $laptop->os_product_key, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('office', 'Office:', array('class'=>'control-label')) }}
            {{ Form::label('office', $laptop->office, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('office_product_key', 'Office Product Key:', array('class'=>'control-label')) }}
            {{ Form::label('office_product_key', $laptop->office_product_key, array('class'=>'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3">
            {{ Form::label('ethernet_mac', 'Ethernet Mac:', array('class'=>'control-label')) }}
            {{ Form::label('ethernet_mac', $laptop->ethernet_mac, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('wireless_mac', 'Wireless Mac:', array('class'=>'control-label')) }}
            {{ Form::label('wireless_mac', $laptop->wireless_mac, array('class'=>'form-control')) }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('condition', 'Condition:', array('class'=>'control-label')) }}<br>
            <!-- {{ Form::label('condition', $laptop->condition, array('class'=>'form-control')) }} -->
            {{ Form::select('condition', array(
                'Brand New'=>'Brand New',
                'Good Condition'=>'Good Condition',
                'Usable but with defect(s)'=>'Usable but with defect(s)',
                'Unusable'=>'Unusable',
                ), $laptop->condition, array('class'=>'form-control chosen-select', 'disabled'=>'true'))
            }}
        </div>
        <div class="col-xs-3">
            {{ Form::label('status', 'Status:', array('class'=>'control-label')) }}<br>
            <!-- {{ Form::label('status', $laptop->status, array('class'=>'form-control')) }} -->
			{{ Form::select('status', array(
				'Available'=>'Available',
				'Issued'=>'Issued'
				), $laptop->status, array('class'=>'form-control chosen-select', 'disabled'=>'true'))
			}}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            {{ Form::label('accessories', 'Accessories:', array('class'=>'control-label')) }}
            {{ Form::textarea('accessories', $laptop->accessories, array('class'=>'form-control', 'disabled'=>'true', 'placeholder'=>'e.g. Backpack, Mouse, USB Flash Drive, etc.')) }}
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="issue_modal" tabindex="-1" role="dialog" aria-labelledby="#issue_modal_label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="issue_modal_label">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

 </form>

@stop