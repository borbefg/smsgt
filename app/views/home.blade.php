<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>IT Management System</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/app.css') }}">
		<style type="text/css">
			@media (min-width: 768px) {
				body {
					background: url("{{ URL::asset('assets/images/background.jpg'); }}");
					background-size: 180%;
				}
			}
			.main.container {
				margin-top: 12%;
			}
			#login_form {
				background-color: rgba(255,255,255,0.2);
				border: 1px solid rgba(221,221,221, 0.2);
				padding: 35px 45px 15px 45px;
				border-radius: 10px;
				-moz-box-shadow: 0 0 15px #ddd;
					 box-shadow: 0 0 15px #ddd;
			}
			#login_form h1 {
				font-size: 5em;
				margin: 0px;
				padding: 0px;
				margin-bottom: 20px;
			}
		</style>
	</head>

	<body>
		<div class="main container">
			<div class="row">
				<div id="login_form" class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
					<h1 class="text-center"><span class="fa fa-lock fa-fw"></span></h1>
					<form class="form-horizontal" action="auth/login" method="POST">
						<div class="form-group input-group">
							<label for="user_name" class="input-group-addon"><i class="fa fa-user fa-fw"></i></label>
							<input type="text" name="user_name" class="form-control" placeholder="Username">
						</div>
						<div class="form-group input-group">
							<label for="user_pass" class="input-group-addon"><i class="fa fa-lock fa-fw"></i></label>
							<input type="password" name="user_pass" class="form-control" placeholder="Password">
						</div>
						<div class="form-group">
							<!-- <button type="submit" class="btn btn-info btn-block">Login</button> -->
							<a href="dashboard" class="btn btn-primary btn-block">Login</a>
						</div>
					</form>
				</div>
			</div>
		</div>

		<script src="{{ URL::asset('assets/js/jquery-1.11.1.min.js') }}"></script>
		<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
	</body>
</html>
