<!doctype html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <title>IT Management System</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/admin.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/app.css') }}">
        
        <style type="text/css">
            body > .header .navbar {
                border-bottom: 1px solid #fff;
            }
            .left-side span{
                position: absolute;
                padding-left: 10px;
            }
            .navbar-nav > .user-menu > .dropdown-toggle {
                max-height: 49px;
            }
            .btn-skin {
                background-color: transparent;
                border: none;
                padding-top: 15px;
                padding-bottom: 15px;
            }

            .skin-blue .btn-skin{
                color: rgba(255, 255, 255, 0.8);
            }

            .skin-black .btn-skin{
                color: rgba(0, 0, 0, 0.8);
            }

            .skin-blue .right-side > .content-header {
                max-height: 46px;
                background: #fbfbfb;
                box-shadow: none;
            }
            .container.box.box-solid {
                padding: 25px;
                height: 100%;
            }
            .skin-blue .sidebar > .sidebar-menu > li.active > a {
              border: none;
              z-index: 9999;
              width: 220px;
            }
            .skin-blue .sidebar > .sidebar-menu > li:first-child {
                border-top: none;
            }
        </style>
    </head>
    
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <!-- <a href="javascript:void(0);" class="navbar-btn sidebar-toggle" data-toggle="" role="button"> -->
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a href="/" class="logo">
                    <!-- Add the class icon to your logo image or logo icon to add the margining -->
                    IT Management System
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span>Admin <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{ URL::asset('assets/images/avatar.png') }}" class="img-circle" alt="Avatar" />
                                    <p>
                                        Admin
                                        <small>Website Administrator</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!-- <li class="user-body">
                                    <div class="col-xs-4 text-center"><a href="#">Followers</a></div>
                                    <div class="col-xs-4 text-center"><a href="#">Sales</a></div>
                                    <div class="col-xs-4 text-center"><a href="#">Friends</a></div>
                                </li> -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left"><a href="#" class="btn btn-default btn-flat">Profile</a></div>
                                    <div class="pull-right"><a href="#" class="btn btn-default btn-flat">Sign out</a></div>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <button class="btn-skin"><i class="fa fa-sun-o"></i></button>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar -->
                <section class="sidebar">
                    <!-- sidebar menu -->
                    <ul class="sidebar-menu">
                        <li class="active"><a href="/dashboard" data-toggle="tooltip" data-placement="right" title="Dashboard Overview"><i class="fa fa-dashboard"></i><span class="">Dashboard</span></a></li>
                        <li class=""><a href="/employees" data-toggle="tooltip" data-placement="right" title="Employees"><i class="fa fa-user"></i><span class="">Employees</span></a></li>
                        <li class=""><a href="/laptops" data-toggle="tooltip" data-placement="right" title="Laptops"><i class="fa fa-laptop"></i><span class="">Laptops</span></a></li>
                        <li class=""><a href="/memos" data-toggle="tooltip" data-placement="right" title="Memos"><i class="fa fa-file-text-o"></i><span class="">Memos</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <!-- <section class="content-header">
                    <h1>
                        {{ $heading }}
                        <small>{{ $sub_heading }}</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol>
                </section> -->

                <!-- Main content -->
                <section class="content small">
                    <!-- @if (Session::has('message'))
                        <div class="flash alert">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif -->

                    <div class="container box box-solid">
                        @yield('main')                        
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

		<script src="{{ URL::asset('assets/js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/admin.js') }}"></script>
	    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
	    <script type="text/javascript">
            $(document).ready(function(){
                $('.btn-skin').click(function(){
                    $('body').toggleClass('skin-blue').toggleClass('skin-black');
                });
            });
        </script>
	</body>
</html>