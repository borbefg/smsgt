<?php

class MemosController extends BaseController {

	/**
	 * Memo Repository
	 *
	 * @var Memo
	 */
	protected $memo;

	public function __construct(Memo $memo)
	{
		$this->memo = $memo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$memos = $this->memo->all();
		$heading = 'Memos';
		$sub_heading = 'Logs';
		return View::make('memos.index', compact('memos', 'heading', 'sub_heading'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$heading = 'Memos';
		$sub_heading = 'New Entry';
		return View::make('memos.create', compact('heading', 'sub_heading'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Memo::$rules);

		if ($validation->passes())
		{
			$this->memo->create($input);

			return Redirect::route('memos.index');
		}

		return Redirect::route('memos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$memo = $this->memo->findOrFail($id);
		$heading = 'Memo';
		$sub_heading = 'Detailed Information';
		return View::make('memos.show', compact('memo', 'heading', 'sub_heading'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$memo = $this->memo->find($id);

		if (is_null($memo))
		{
			return Redirect::route('memos.index');
		}
		$heading = 'Memo';
		$sub_heading = 'Edit Information';
		return View::make('memos.edit', compact('memo', 'heading', 'sub_heading'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Memo::$rules);

		if ($validation->passes())
		{
			$memo = $this->memo->find($id);
			$memo->update($input);

			return Redirect::route('memos.show', $id);
		}

		return Redirect::route('memos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->memo->find($id)->delete();

		return Redirect::route('memos.index');
	}

}
