<?php

class LaptopsController extends BaseController {

	/**
	 * Laptop Repository
	 *
	 * @var Laptop
	 */
	protected $laptop;

	public function __construct(Laptop $laptop)
	{
		$this->laptop = $laptop;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$laptops = $this->laptop->all();
		$heading = 'Laptops';
		$sub_heading = 'Inventory';
		return View::make('laptops.index', compact('laptops', 'heading', 'sub_heading'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$heading = 'Laptops';
		$sub_heading = 'New Entry';
		return View::make('laptops.create', compact('heading', 'sub_heading'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Laptop::$rules);

		if ($validation->passes())
		{
			$this->laptop->create($input);
			return Redirect::route('laptops.index');
		}

		return Redirect::route('laptops.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$laptop = $this->laptop->findOrFail($id);
		$heading = 'Laptops';
		$sub_heading = 'Detailed Information';
		return View::make('laptops.show', compact('laptop', 'heading', 'sub_heading'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$laptop = $this->laptop->find($id);

		if (is_null($laptop))
		{
			return Redirect::route('laptops.index');
		}

		$heading = 'Laptops';
		$sub_heading = 'Editing';
		return View::make('laptops.edit', compact('laptop', 'heading', 'sub_heading'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		// $validation = Validator::make($input, Laptop::$rules);

		// if ($validation->passes())
		// {
			$laptop = $this->laptop->find($id);
			$laptop->update($input);

			return Redirect::route('laptops.show', $id);
		// }

		// return Redirect::route('laptops.edit', $id)
		// 	->withInput()
		// 	->withErrors($validation)
		// 	->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->laptop->find($id)->delete();
		return Redirect::route('laptops.index');
	}
}
