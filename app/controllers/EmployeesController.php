<?php

class EmployeesController extends BaseController {

	/**
	 * Employee Repository
	 *
	 * @var Employee
	 */
	protected $employee;

	public function __construct(Employee $employee)
	{
		$this->employee = $employee;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$employees = $this->employee->all();
		$heading = 'Employees';
		$sub_heading = 'Master List';
		return View::make('employees.index', compact('employees', 'heading', 'sub_heading'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$heading = 'Employees';
		$sub_heading = 'New Entry';
		return View::make('employees.create', compact('heading', 'sub_heading'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Employee::$rules);

		if ($validation->passes())
		{
			$this->employee->create($input);

			return Redirect::route('employees.index');
		}

		return Redirect::route('employees.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$employee = $this->employee->findOrFail($id);
		$heading = $employee->last_name.', '.$employee->first_name.' '.$employee->middle_name;
		$sub_heading = 'Detailed Information';
		return View::make('employees.show', compact('employee', 'heading', 'sub_heading'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$employee = $this->employee->find($id);

		if (is_null($employee))
		{
			return Redirect::route('employees.index');
		}
		$heading = $employee->last_name.', '.$employee->first_name.' '.$employee->middle_name;
		$sub_heading = 'Edit';
		return View::make('employees.edit', compact('employee', 'heading', 'sub_heading'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Employee::$rules);

		if ($validation->passes())
		{
			$employee = $this->employee->find($id);
			$employee->update($input);

			return Redirect::route('employees.show', $id);
		}

		return Redirect::route('employees.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->employee->find($id)->delete();
		return Redirect::route('employees.index');
	}

}
