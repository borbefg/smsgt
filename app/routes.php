<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showWelcome');
Route::get('dashboard', function(){
	$heading = 'Dashboard';
	$sub_heading = 'Overview';
	return View::make('dashboard', compact('heading', 'sub_heading'));

});
Route::resource('employees', 'EmployeesController');
Route::resource('laptops', 'LaptopsController');
Route::resource('memos', 'MemosController');