$(document).ready(function(){
	// Datepicker
	$(".datepicker").datepicker({
		format: 'dd M yyyy',
		autoclose: true,
		todayBtn: true
	});

	$("input[name='contract_period']").datepicker({
		format: 'dd M yyyy',
		todayBtn: true,
		multidate: true,
		multidateSeparator: ' - ',
	});

	// Chosen Select
	$(".chosen-select").chosen();

	// DataTables
	$(".data-table").dataTable();

});